FROM microsoft/dotnet:2.1-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 80

FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /src
COPY ["MicroServiceDeneme/MicroServiceDeneme.csproj", "MicroServiceDeneme/"]
RUN dotnet restore "MicroServiceDeneme/MicroServiceDeneme.csproj"
COPY . .
WORKDIR "/src/MicroServiceDeneme"
RUN dotnet build "MicroServiceDeneme.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "MicroServiceDeneme.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "MicroServiceDeneme.dll"]