﻿using MicroServiceDeneme.Models;
using Microsoft.EntityFrameworkCore;

namespace MicroServiceDeneme.DBContexts
{
    public class ProductContext : DbContext
    {

        public ProductContext(DbContextOptions<ProductContext> options) : base(options)
        {

        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasData(
                new Category
                {
                    Id = 0,
                    Name = "Electronics",
                    Description = "Electronic Items",
                },
                new Category
                {
                    Id = 1,
                    Name = "Clothes",
                    Description = "Dresses",
                },
                new Category
                {
                    Id = 1,
                    Name = "Grocery",
                    Description = "Grocery Items",
                }
            );
        }
    }
}
